# Generated by carnix 0.10.0: carnix generate-nix --src .
{ lib, buildPlatform, buildRustCrate, buildRustCrateHelpers, cratesIO, fetchgit
, bindgen, gnunet, libclang }:
with buildRustCrateHelpers;
let inherit (lib.lists) fold;
    inherit (lib.attrsets) recursiveUpdate;
in
rec {
  crates = cratesIO // rec {
# gnunet-bindgen-0.1.0

    crates.gnunet_bindgen."0.1.0" = deps: { features?(features_.gnunet_bindgen."0.1.0" deps {}) }: buildRustCrate {
      crateName = "gnunet-bindgen";
      version = "0.1.0";
      authors = [  ];
      edition = "2018";
      src = include [ "Cargo.toml" " gnunet-bindgen" ] ./.;
      workspace_member = "gnunet-bindgen";
      build = "build.rs";

      buildInputs = [ bindgen gnunet ];
      buildDependencies = mapFeatures features ([
        (cratesIO.crates."bindgen"."${deps."gnunet_bindgen"."0.1.0"."bindgen"}" deps)
      ]);

      preConfigure = ''export LIBCLANG_PATH="${libclang}/lib"'';
    };
    features_.gnunet_bindgen."0.1.0" = deps: f: updateFeatures f (rec {
      bindgen."${deps.gnunet_bindgen."0.1.0".bindgen}".default = true;
      gnunet_bindgen."0.1.0".default = (f.gnunet_bindgen."0.1.0".default or true);
    }) [
      (cratesIO.features_.bindgen."${deps."gnunet_bindgen"."0.1.0"."bindgen"}" deps)
    ];


# end
# gnunet-rs-0.1.0

    crates.gnunet_rs."0.1.0" = deps: { features?(features_.gnunet_rs."0.1.0" deps {}) }: buildRustCrate {
      crateName = "gnunet-rs";
      version = "0.1.0";
      authors = [ "Dmitriy Volkov <wldhx@wldhx.me>" ];
      edition = "2018";
      src = include [ "Cargo.toml" " gnunet-rs" ] ./.;
      workspace_member = "gnunet-rs";
      dependencies = mapFeatures features ([
        (cratesIO.crates."bimap"."${deps."gnunet_rs"."0.1.0"."bimap"}" deps)
        (crates."gnunet_bindgen"."${deps."gnunet_rs"."0.1.0"."gnunet_bindgen"}" deps)
        (cratesIO.crates."mio"."${deps."gnunet_rs"."0.1.0"."mio"}" deps)
      ]);
    };
    features_.gnunet_rs."0.1.0" = deps: f: updateFeatures f (rec {
      bimap."${deps.gnunet_rs."0.1.0".bimap}".default = true;
      gnunet_bindgen."${deps.gnunet_rs."0.1.0".gnunet_bindgen}".default = true;
      gnunet_rs."0.1.0".default = (f.gnunet_rs."0.1.0".default or true);
      mio."${deps.gnunet_rs."0.1.0".mio}".default = true;
    }) [
      (cratesIO.features_.bimap."${deps."gnunet_rs"."0.1.0"."bimap"}" deps)
      (features_.gnunet_bindgen."${deps."gnunet_rs"."0.1.0"."gnunet_bindgen"}" deps)
      (cratesIO.features_.mio."${deps."gnunet_rs"."0.1.0"."mio"}" deps)
    ];


# end

  };

  gnunet_bindgen = crates.crates.gnunet_bindgen."0.1.0" deps;
  gnunet_rs = crates.crates.gnunet_rs."0.1.0" deps;
  __all = [ (gnunet_bindgen {}) (gnunet_rs {}) ];
  deps.aho_corasick."0.7.3" = {
    memchr = "2.2.0";
  };
  deps.ansi_term."0.11.0" = {
    winapi = "0.3.7";
  };
  deps.atty."0.2.11" = {
    termion = "1.5.2";
    libc = "0.2.54";
    winapi = "0.3.7";
  };
  deps.autocfg."0.1.2" = {};
  deps.backtrace."0.3.15" = {
    cfg_if = "0.1.7";
    rustc_demangle = "0.1.14";
    autocfg = "0.1.2";
    backtrace_sys = "0.1.28";
    libc = "0.2.54";
    winapi = "0.3.7";
  };
  deps.backtrace_sys."0.1.28" = {
    libc = "0.2.54";
    cc = "1.0.36";
  };
  deps.bimap."0.3.1" = {
    cfg_if = "0.1.7";
  };
  deps.bindgen."0.48.1" = {
    bitflags = "1.0.5";
    cexpr = "0.3.5";
    cfg_if = "0.1.7";
    clang_sys = "0.26.4";
    clap = "2.33.0";
    env_logger = "0.6.1";
    hashbrown = "0.1.8";
    lazy_static = "1.3.0";
    log = "0.4.6";
    peeking_take_while = "0.1.2";
    proc_macro2 = "0.4.29";
    quote = "0.6.12";
    regex = "1.1.6";
    which = "2.0.1";
  };
  deps.bitflags."1.0.5" = {};
  deps.byteorder."1.3.1" = {};
  deps.cc."1.0.36" = {};
  deps.cexpr."0.3.5" = {
    nom = "4.2.3";
  };
  deps.cfg_if."0.1.7" = {};
  deps.clang_sys."0.26.4" = {
    glob = "0.2.11";
    libc = "0.2.54";
    libloading = "0.5.0";
  };
  deps.clap."2.33.0" = {
    atty = "0.2.11";
    bitflags = "1.0.5";
    strsim = "0.8.0";
    textwrap = "0.11.0";
    unicode_width = "0.1.5";
    vec_map = "0.8.1";
    ansi_term = "0.11.0";
  };
  deps.env_logger."0.6.1" = {
    atty = "0.2.11";
    humantime = "1.2.0";
    log = "0.4.6";
    regex = "1.1.6";
    termcolor = "1.0.4";
  };
  deps.failure."0.1.5" = {
    backtrace = "0.3.15";
  };
  deps.fuchsia_zircon."0.3.3" = {
    bitflags = "1.0.5";
    fuchsia_zircon_sys = "0.3.3";
  };
  deps.fuchsia_zircon_sys."0.3.3" = {};
  deps.glob."0.2.11" = {};
  deps.gnunet_bindgen."0.1.0" = {
    bindgen = "0.48.1";
  };
  deps.gnunet_rs."0.1.0" = {
    bimap = "0.3.1";
    gnunet_bindgen = "0.1.0";
    mio = "0.6.16";
  };
  deps.hashbrown."0.1.8" = {
    byteorder = "1.3.1";
    scopeguard = "0.3.3";
  };
  deps.humantime."1.2.0" = {
    quick_error = "1.2.2";
  };
  deps.iovec."0.1.2" = {
    libc = "0.2.54";
    winapi = "0.2.8";
  };
  deps.kernel32_sys."0.2.2" = {
    winapi = "0.2.8";
    winapi_build = "0.1.1";
  };
  deps.lazy_static."1.3.0" = {};
  deps.lazycell."1.2.1" = {};
  deps.libc."0.2.54" = {};
  deps.libloading."0.5.0" = {
    cc = "1.0.36";
    winapi = "0.3.7";
  };
  deps.log."0.4.6" = {
    cfg_if = "0.1.7";
  };
  deps.memchr."2.2.0" = {};
  deps.mio."0.6.16" = {
    iovec = "0.1.2";
    lazycell = "1.2.1";
    log = "0.4.6";
    net2 = "0.2.33";
    slab = "0.4.2";
    fuchsia_zircon = "0.3.3";
    fuchsia_zircon_sys = "0.3.3";
    libc = "0.2.54";
    kernel32_sys = "0.2.2";
    miow = "0.2.1";
    winapi = "0.2.8";
  };
  deps.miow."0.2.1" = {
    kernel32_sys = "0.2.2";
    net2 = "0.2.33";
    winapi = "0.2.8";
    ws2_32_sys = "0.2.1";
  };
  deps.net2."0.2.33" = {
    cfg_if = "0.1.7";
    libc = "0.2.54";
    winapi = "0.3.7";
  };
  deps.nom."4.2.3" = {
    memchr = "2.2.0";
    version_check = "0.1.5";
  };
  deps.numtoa."0.1.0" = {};
  deps.peeking_take_while."0.1.2" = {};
  deps.proc_macro2."0.4.29" = {
    unicode_xid = "0.1.0";
  };
  deps.quick_error."1.2.2" = {};
  deps.quote."0.6.12" = {
    proc_macro2 = "0.4.29";
  };
  deps.redox_syscall."0.1.54" = {};
  deps.redox_termios."0.1.1" = {
    redox_syscall = "0.1.54";
  };
  deps.regex."1.1.6" = {
    aho_corasick = "0.7.3";
    memchr = "2.2.0";
    regex_syntax = "0.6.6";
    thread_local = "0.3.6";
    utf8_ranges = "1.0.2";
  };
  deps.regex_syntax."0.6.6" = {
    ucd_util = "0.1.3";
  };
  deps.rustc_demangle."0.1.14" = {};
  deps.scopeguard."0.3.3" = {};
  deps.slab."0.4.2" = {};
  deps.strsim."0.8.0" = {};
  deps.termcolor."1.0.4" = {
    wincolor = "1.0.1";
  };
  deps.termion."1.5.2" = {
    numtoa = "0.1.0";
    libc = "0.2.54";
    redox_syscall = "0.1.54";
    redox_termios = "0.1.1";
  };
  deps.textwrap."0.11.0" = {
    unicode_width = "0.1.5";
  };
  deps.thread_local."0.3.6" = {
    lazy_static = "1.3.0";
  };
  deps.ucd_util."0.1.3" = {};
  deps.unicode_width."0.1.5" = {};
  deps.unicode_xid."0.1.0" = {};
  deps.utf8_ranges."1.0.2" = {};
  deps.vec_map."0.8.1" = {};
  deps.version_check."0.1.5" = {};
  deps.which."2.0.1" = {
    failure = "0.1.5";
    libc = "0.2.54";
  };
  deps.winapi."0.2.8" = {};
  deps.winapi."0.3.7" = {
    winapi_i686_pc_windows_gnu = "0.4.0";
    winapi_x86_64_pc_windows_gnu = "0.4.0";
  };
  deps.winapi_build."0.1.1" = {};
  deps.winapi_i686_pc_windows_gnu."0.4.0" = {};
  deps.winapi_util."0.1.2" = {
    winapi = "0.3.7";
  };
  deps.winapi_x86_64_pc_windows_gnu."0.4.0" = {};
  deps.wincolor."1.0.1" = {
    winapi = "0.3.7";
    winapi_util = "0.1.2";
  };
  deps.ws2_32_sys."0.2.1" = {
    winapi = "0.2.8";
    winapi_build = "0.1.1";
  };
}
