// Autotools guards in gnunet_common.h
#define HAVE_SYS_SOCKET_H 1
#define HAVE_NETINET_IN_H 1
#define HAVE_STDINT_H 1
#define HAVE_STDARG_H 1
#define HAVE_BYTESWAP_H 1

#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_scheduler_lib.h>
#include <gnunet/gnunet_transport_hello_service.h>
