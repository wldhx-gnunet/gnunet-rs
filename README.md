# gnunet-rs

## Building

Directory layout should be as such:
```
/gnunet # some version of gnunet which builds with nix-build
        # we'll use its shared libraries
/gnunet.compat-rs # gnunet with includes patched for bindgen
/gnunet-rs # this repo
```

`gnunet-bindgen/build.rs` and `shell.nix` use relative paths.

Which translates to something like this:

```sh
git clone https://gitlab.com/wldhx-gnunet/gnunet.git
git clone https://gitlab.com/wldhx-gnunet/gnunet.compat-rs.git
git clone https://gitlab.com/wldhx-gnunet/gnunet-rs.git
cd gnunet.compat-rs
mkdir dist
nix-shell
./bootstrap && ./configure --prefix=$PWD/dist
cd src/include
make install
cd ../../../gnunet-rs
nix-shell
cargo run -p gnunet-rs --example hash
```

Henceworth, `gnunet-rs$ nix-shell` should be enough for daily development.
