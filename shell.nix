with import <nixpkgs> {};
let
  gnunet = (pkgs.callPackage ../gnunet/gnunet-dev.nix { }).dev;
in
pkgs.stdenv.mkDerivation {
  name = "gnunet-rs-env";
  buildInputs = [ rustChannels.stable.rust rustChannels.stable.cargo gnunet llvmPackages.libclang.lib ];
  shellHook = "export LIBCLANG_PATH=${llvmPackages.libclang.lib}/lib";
}
