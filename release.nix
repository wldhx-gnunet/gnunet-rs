{ pkgs ? import <nixpkgs> { overlays = [(import <gnunet-nix/overlay.nix>)]; } }:
let
  cratesIO = (pkgs.callPackage ./crates-io.nix { });
  cargo = pkgs.callPackage ./Cargo.nix {
    inherit cratesIO;
    bindgen = pkgs.rust-bindgen;
    libclang = pkgs.llvmPackages.libclang.lib;
    gnunet = pkgs.gnunet;
  };
in {
  gnunet-bindgen = cargo.gnunet_bindgen { };
  gnunet-rs = cargo.gnunet_rs { };
}
