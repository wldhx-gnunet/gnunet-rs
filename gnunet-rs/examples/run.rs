extern crate gnunet_bindgen;

use std::alloc::{alloc, dealloc, Layout};
use std::ptr::{read, null_mut};
use std::ffi::c_void;
use std::ffi::CString;
use gnunet_bindgen::*;

static mut hello_handle_p: *mut GNUNET_TRANSPORT_HelloGetHandle = null_mut() as *mut GNUNET_TRANSPORT_HelloGetHandle;

unsafe extern "C" fn hello_cb(cls: *mut std::ffi::c_void, hello: *const GNUNET_MessageHeader) {
    if hello.is_null() { GNUNET_SCHEDULER_shutdown(); return; }
    let our_hello = (GNUNET_copy_message(hello) as *mut GNUNET_HELLO_Message).as_ref().unwrap();

    GNUNET_TRANSPORT_hello_get_cancel(hello_handle_p);

    let pid = {
        let layout = Layout::new::<GNUNET_PeerIdentity>();
        let ptr = alloc(layout) as *mut GNUNET_PeerIdentity;
        GNUNET_HELLO_get_id(our_hello, ptr);
        ptr.as_ref().unwrap()
    };
    println!("{:?}", &pid);
/*
        putMVar our_pid =<< peek our_pid'
*/
}

unsafe extern "C" fn cb(cls: *mut std::ffi::c_void) {
    let cfg = cls as *const GNUNET_CONFIGURATION_Handle;

    hello_handle_p = GNUNET_TRANSPORT_hello_get(cfg, GNUNET_TRANSPORT_AddressClass_GNUNET_TRANSPORT_AC_ANY, Some(hello_cb), null_mut());
}

fn main() {
    unsafe {
        {
            let name = CString::from_vec_unchecked(b"run-example".to_vec()).into_raw();
            let loglevel = CString::from_vec_unchecked(b"DEBUG".to_vec()).into_raw();
            GNUNET_log_setup(name, loglevel, null_mut());

            // go go Drop!
            let _name = CString::from_raw(name);
            let _loglevel = CString::from_raw(loglevel);
        }

        let cfg = GNUNET_CONFIGURATION_create();
        {
            let path = CString::from_vec_unchecked(b"/tmp/gnunet.conf".to_vec()).into_raw();
            GNUNET_CONFIGURATION_load(cfg, path);
            let _path = CString::from_raw(path);
        }

        GNUNET_SCHEDULER_run(Some(cb), cfg as *mut c_void);
    }
}
