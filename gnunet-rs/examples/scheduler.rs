extern crate gnunet_bindgen;

use bimap::BiHashMap;
use gnunet_bindgen::*;
use mio::{Events, Poll, PollOpt, Ready, Token};
use std::alloc::{alloc, dealloc, Layout};
use std::collections::HashMap;
use std::ffi::c_void;
use std::ffi::CString;
use std::ptr::{null_mut, read};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::time::Duration;

static mut hello_handle_p: *mut GNUNET_TRANSPORT_HelloGetHandle =
    null_mut() as *mut GNUNET_TRANSPORT_HelloGetHandle;

unsafe extern "C" fn hello_cb(cls: *mut std::ffi::c_void, hello: *const GNUNET_MessageHeader) {
    if hello.is_null() {
        GNUNET_SCHEDULER_shutdown();
        return;
    }
    let our_hello = (GNUNET_copy_message(hello) as *mut GNUNET_HELLO_Message)
        .as_ref()
        .unwrap();

    GNUNET_TRANSPORT_hello_get_cancel(hello_handle_p);

    let pid = {
        let layout = Layout::new::<GNUNET_PeerIdentity>();
        let ptr = alloc(layout) as *mut GNUNET_PeerIdentity;
        GNUNET_HELLO_get_id(our_hello, ptr);
        ptr.as_ref().unwrap()
    };
    println!("{:?}", &pid);
    /*
            putMVar our_pid =<< peek our_pid'
    */
}

unsafe extern "C" fn cb(cls: *mut std::ffi::c_void) {
    let cfg = cls as *const GNUNET_CONFIGURATION_Handle;

    hello_handle_p = GNUNET_TRANSPORT_hello_get(
        cfg,
        GNUNET_TRANSPORT_AddressClass_GNUNET_TRANSPORT_AC_ANY,
        Some(hello_cb),
        null_mut(),
    );
}

fn ready_from_et(et: uint) -> Ready {
    match et {
        GNUNET_SCHEDULER_EventType_GNUNET_SCHEDULER_ET_IN => Ready::readable(),
        GNUNET_SCHEDULER_EventType_GNUNET_SCHEDULER_ET_OUT => Ready::writable(),
        _ => unimplemented!(),
    }
}

unsafe extern "C" fn add(
    cls: *mut ::std::os::raw::c_void,
    task: *mut GNUNET_SCHEDULER_Task,
    fdi: *mut GNUNET_SCHEDULER_FdInfo,
) -> ::std::os::raw::c_int {
    println!("driver: add: {:?} {:?} {:?}", cls, task, *fdi);
    let mut ctx = &mut *(cls as *mut SchedulerCtx); // FIXME: *mut => *const?

    let a_ = ctx.taskfdi2token.clone();
    let known_such_fdi = ctx
        .taskfdi2token
        .left_values()
        .map(|x| {
            x.into_iter().find(
                |(_, known_fdi, _)| (*fdi).sock == (**known_fdi).sock, /*&& (*fdi).et == (**known_fdi).et*/
            )
        })
        .fold(None, Option::or); // FIXME: gets at most one

    let ready = ready_from_et((*fdi).et);

    println!("driver: add: ready: {:?}", ready);
    println!(
        "driver: add: fdi_yet_unknown: {:?}",
        known_such_fdi.is_none()
    );

    println!("driver: add: taskfdi2token before: {:?}", ctx.taskfdi2token);

    if known_such_fdi.is_none() {
        let new_token = Token(ctx.next_token.fetch_add(1, Ordering::SeqCst));
        println!("driver: add: token: {:?}", new_token);

        ctx.taskfdi2token
            .insert_no_overwrite(vec![(task, fdi, (*fdi).et)], new_token)
            .unwrap();

        ctx.poll
            .register(
                &mio::unix::EventedFd(&(*fdi).sock),
                new_token,
                ready,
                PollOpt::edge(),
            )
            .unwrap();
    } else {
        let known_such_fdi = known_such_fdi.unwrap();
        let known_such_fdi_token = a_.get_by_left(&vec![*known_such_fdi]).unwrap();
        println!(
            "driver: add: known_such_fdi: {:?} {:?}",
            known_such_fdi, known_such_fdi_token
        );
        let ready_known = ready_from_et(known_such_fdi.2);
        println!(
            "driver: add: ready | ready_known : {:?}",
            ready | ready_known
        );

        let mut b = ctx
            .taskfdi2token
            .get_by_right(known_such_fdi_token)
            .unwrap()
            .clone();
        b.push((task, fdi, (*fdi).et));
        ctx.taskfdi2token.insert(b.to_vec(), *known_such_fdi_token);

        ctx.poll
            .reregister(
                &mio::unix::EventedFd(&(*fdi).sock),
                *known_such_fdi_token,
                ready | ready_known,
                PollOpt::edge(),
            )
            .unwrap();
    }

    println!("driver: add: taskfdi2token after: {:?}", ctx.taskfdi2token);

    1 // GNUNET_OK
}

unsafe extern "C" fn del(
    cls: *mut ::std::os::raw::c_void,
    task: *mut GNUNET_SCHEDULER_Task,
) -> ::std::os::raw::c_int {
    println!("driver: del: {:?} {:?}", cls, task);
    let mut ctx = &mut *(cls as *mut SchedulerCtx); // FIXME: *mut => *const?

    let a = ctx.taskfdi2token.clone();
    let (this_task, this_fdi, _, this_token) = a
        .iter()
        .map(|(lhs, t)| {
            lhs.into_iter()
                .map(|(a, b, c)| (a, b, c, t))
                .find(|(this_task, this_fdi, _, this_token)| task == **this_task as *mut _)
        })
        .fold(None, Option::or)
        .unwrap(); // FIXME: gets at most one // FIXME: unwrap() => GNUNET_SYSERR
    let lhs = ctx.taskfdi2token.get_by_right(this_token).unwrap();
    let lhs_new = lhs
        .into_iter()
        .filter(|(this_task, _, _)| task != *this_task as *mut _)
        .cloned()
        .collect::<Vec<_>>();
    if !lhs_new.is_empty() {
        ctx.taskfdi2token.insert(lhs_new, *this_token);
    } else {
        ctx.taskfdi2token.remove_by_right(this_token);
    }

    println!("driver: del: after del: (tfdi2tok) {:?}", ctx.taskfdi2token);

    1 // GNUNET_OK
}

unsafe extern "C" fn set_wakeup(cls: *mut std::ffi::c_void, dt: GNUNET_TIME_Absolute) {
    println!("driver: set_wakeup: {:?} {:?}", cls, dt);
    let mut ctx = &mut *(cls as *mut SchedulerCtx); // FIXME: *mut => *const?

    if !(dt.abs_value_us == GNUNET_TIME_absolute_get_forever_().abs_value_us) {
        println!(
            "driver: set_wakeup: {} - {}",
            GNUNET_TIME_absolute_get().abs_value_us,
            dt.abs_value_us
        );
        ctx.till_timeout = Some(Duration::from_micros(std::cmp::min(
            GNUNET_TIME_absolute_get().abs_value_us - dt.abs_value_us,
            u64::max_value(),
        )));
    } else {
        ctx.till_timeout = None;
    }

    println!("driver: set_wakeup: {:?}", ctx.till_timeout);
}

struct SchedulerCtx {
    taskfdi2token: BiHashMap<
        Vec<(
            *const GNUNET_SCHEDULER_Task,
            *mut GNUNET_SCHEDULER_FdInfo,
            u32,
        )>,
        Token,
    >,
    next_token: AtomicUsize,
    poll: Poll,
    till_timeout: Option<Duration>,
}
// TODO: I really didn't think whether it's safe
unsafe impl Send for SchedulerCtx {}
unsafe impl Sync for SchedulerCtx {}

#[derive(Copy, Clone)]
struct Configuration(*const GNUNET_CONFIGURATION_Handle);
unsafe impl Send for Configuration {}
unsafe impl Sync for Configuration {}

struct Scheduler(*mut GNUNET_SCHEDULER_Handle);
unsafe impl Send for Scheduler {}
unsafe impl Sync for Scheduler {}

fn main() {
    unsafe {
        {
            let name = CString::from_vec_unchecked(b"run-example".to_vec()).into_raw();
            let loglevel = CString::from_vec_unchecked(b"DEBUG".to_vec()).into_raw();
            GNUNET_log_setup(name, loglevel, null_mut());

            // go go Drop!
            let _name = CString::from_raw(name);
            let _loglevel = CString::from_raw(loglevel);
        }

        let cfg = GNUNET_CONFIGURATION_create();
        {
            let path = CString::from_vec_unchecked(b"/tmp/gnunet.conf".to_vec()).into_raw();
            GNUNET_CONFIGURATION_load(cfg, path);
            let _path = CString::from_raw(path);
        }

        let mut taskfdi2token = BiHashMap::new();
        let next_token = AtomicUsize::new(0);

        let poll = Poll::new().unwrap();
        let mut events = Events::with_capacity(1024);

        let ctx = SchedulerCtx {
            taskfdi2token,
            next_token,
            poll,
            till_timeout: None,
        };
        let mut ctx_p = Box::new(ctx);

        let sched_ = Box::new(GNUNET_SCHEDULER_Driver {
            cls: &*ctx_p as *const SchedulerCtx as *mut c_void,
            add: Some(add),
            del: Some(del),
            set_wakeup: Some(set_wakeup),
        });
        let sched = Scheduler(GNUNET_SCHEDULER_driver_init(&*sched_));

        hello_handle_p = GNUNET_TRANSPORT_hello_get(
            cfg,
            GNUNET_TRANSPORT_AddressClass_GNUNET_TRANSPORT_AC_ANY,
            Some(hello_cb),
            null_mut(),
        );

        println!("{} {:?}", "main", ctx_p.taskfdi2token);
        while {
            ctx_p.poll.poll(&mut events, ctx_p.till_timeout).unwrap();
            if events.is_empty() {
                println!("driver: loop: taskfdi2token: {:?}", ctx_p.taskfdi2token);
                while { GNUNET_SCHEDULER_do_work(sched.0) != 0 } {}
            }

            for event in &events {
                println!("{}", "driver: ev_cb");
                println!(
                    "driver: ev_cb: event: (evt,tfdi2tok) {:?} {:?}",
                    &event, ctx_p.taskfdi2token
                );

                if let Some(lhs) = ctx_p.taskfdi2token.get_by_right(&event.token()) {
                    // Also viable to poll.register(PollOpt::oneshot) in `add` and reregister here.

                    for (task, mut fdi, et) in lhs {
                        let ready = ready_from_et(*et);

                        println!(
                            "driver: ev_cb: get from state: (evt,ready) {:?} {:?}",
                            &event, ready
                        );

                        if event.readiness().contains(ready) {
                            (*fdi).et = *et;

                            println!(
                                "driver: ev_cb: task_ready: (task,fdi) {:?} {:?}",
                                *task, *fdi
                            );

                            GNUNET_SCHEDULER_task_ready(*task as *mut GNUNET_SCHEDULER_Task, fdi);

                            while { GNUNET_SCHEDULER_do_work(sched.0) != 0 } {}
                        }
                    }
                } else {
                    panic!("driver: ev_cb: notification for unkown event!: {:?}", event);
                }
            }

            !(ctx_p.taskfdi2token.is_empty() && ctx_p.till_timeout.is_none())
        } {}

        GNUNET_SCHEDULER_driver_done(sched.0);
    }
}
