extern crate gnunet_bindgen;

fn main() {
    unsafe {
        let a = gnunet_bindgen::ntohs(123);
        println!("{}", a);
    }
}
