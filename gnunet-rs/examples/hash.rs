extern crate gnunet_bindgen;

use std::alloc::{alloc, dealloc, Layout};
use std::ptr::{read};
use std::ffi::c_void;

fn main() {
    unsafe {
        let a = "abc";

        let layout = Layout::new::<gnunet_bindgen::GNUNET_HashCode>();
        let ptr = alloc(layout) as *mut gnunet_bindgen::GNUNET_HashCode;

        gnunet_bindgen::GNUNET_CRYPTO_hash(a.as_ptr() as *mut c_void, a.len(), ptr);

        let h = read(ptr);
        println!("{:?}", &h);

        dealloc(ptr as *mut u8, layout);
    }
}
